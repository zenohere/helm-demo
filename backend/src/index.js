const Koa = require('koa');
const Router = require('@koa/router');

const app = new Koa();
const router = new Router();
const port = process.env.port || 3003

router.get('/getUsers', (ctx, next) => {
  // ctx.router available
  ctx.body = 'hahaha'
});

router.get('/getEnv', (ctx, next) => {
  // ctx.router available
  ctx.body = process.env.info
});

app
  .use(router.routes())
  .use(router.allowedMethods());

app.listen(port, () => {
  console.log('===================')
  console.log('start.....' + port)
  console.log('===================')
});
